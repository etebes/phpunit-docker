<?php

/**
 * Description of MyCalculatorTest
 *
 * @author Emiliano Tebes
 */
include 'MyCalculator.php';

use PHPUnit\Framework\TestCase;

class MyCalculatorTest extends TestCase {

    public function test_sumar_con2y3_retorna5() {
        $suma = MyCalculator::sumar(2, 3);
        $this->assertEquals(5, $suma);
    }

}
