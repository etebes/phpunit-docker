# Testeando PHP con phpunit y docker #

## Introducción ##
* Este es un proyecto para probar unitariamente clases de php, usando phpunit mediante una imagen de docker.

## Requisitos para ejecutar##
* Tener instalado docker

## Ejecución ##
* Vamos a usar la siguiente [imagen de docker](https://github.com/JulienBreux/phpunit-docker) que ya tiene instalado phpunit.
* Clonar el proyecto
* Abrir una ventana de comandos y situarse en la raiz del mismo.
* La primera vez ejecutar 
```
#!cmd

docker pull phpunit/phpunit
```

* En windows ejecutar: 
```
#!cmd

docker run -v [PATH_ABSOLUTO_PROYECTO]:/app --rm phpunit/phpunit tests
```

ejemplo:

```
#!cmd

docker run -v //E/documentos/NetBeansProjects/phpunit-docker:/app --rm phpunit/phpunit tests
```

## Links de interés ##
* [Documentación inicial de phpunit](https://phpunit.de/getting-started.html)
* [Organizar los tests de phpunit](https://phpunit.de/manual/current/en/organizing-tests.html)
* [Imagen de docker con phpunit](https://github.com/JulienBreux/phpunit-docker)